import { mapState, mapMutations } from "vuex";
export default (storeName, modelName) => ({
  computed: {
    ...mapState(storeName, ["local"]),
    model: {
      get() {
        return this.local[modelName];
      },
      set(value) {
        this.update({ name: modelName, value });
      },
    },
  },
  methods: {
    ...mapMutations(storeName, { update: "UPDATE_ONE" }),
  },
});
