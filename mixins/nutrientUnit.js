export default {
  methods: {
    unit(nutrientName) {
      switch (nutrientName) {
        case "calories":
          return this.$t("shared.unit.kcal");
        case "proteins":
        case "lipids":
        case "carbohydrates":
          return this.$tc("shared.unit.gram", 2);
      }
    },
  },
};
