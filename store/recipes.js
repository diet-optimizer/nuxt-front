import recipes from "@/data/recipes.json";
import getRecipe from "@/gql/recipe.gql";

export const state = () => ({
  list: {},
});

export const getters = {
  likedRecipesIds(state) {
    return Object.keys(state.list).filter((id) => state.list[id].liked);
  },
};

const splitIngredients = (ingredientsString) => {
  const withDoubleQuote = ingredientsString.replace(/'/g, '"');
  const ingredients = JSON.parse(withDoubleQuote);
  return ingredients;
};

export const actions = {
  async fetchRecipeByID({ state, commit }, id) {
    if (!(id in state.list)) {
      const apolloClient = this.app.apolloProvider.defaultClient;
      const recipe = await apolloClient
        .query({
          query: getRecipe,
          variables: { id },
        })
        .then(({ data }) => data && data.recipe);

      let formattedRecipe;
      if (!recipe) {
        formattedRecipe = recipes[id]; // fetch
      } else {
        formattedRecipe = {
          name: recipe.recipeName,
          liked: recipe.liked,
          nutrients: {
            calories: recipe.TotalCalories,
            carbohydrates: recipe.Carbohydrates,
            proteins: recipe.Protein,
            lipids: recipe.TotalFat,
          },
          ingredients: splitIngredients(recipe.recipeIngre),
          prepTime: 30, // arbitrary
          price: recipe.price,
          servings: recipe.servings,
        };
      }

      if (formattedRecipe === undefined) {
        throw new Error(`Can't find recipe with id: ${id}`);
      }
      commit("ADD_RECIPE", { id, recipe: formattedRecipe });
    }
    return state.list[id];
  },
  fetchLikedRecipes({ state, commit }) {
    Object.entries(recipes)
      .filter(([id, recipe]) => recipe.liked)
      .forEach(([id, recipe]) => {
        if (!(id in state.list)) {
          commit("ADD_RECIPE", { id, recipe });
        }
      });
  },
};

export const mutations = {
  ADD_RECIPE(state, { id, recipe }) {
    state.list[id] = recipe;
    state.list = { ...state.list };
  },
};
