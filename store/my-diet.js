export const state = () => ({
  local: {},
  remote: {},
});

export const getters = {
  areSaved(state) {
    const names = Object.keys(state.local);
    return names.every((name) => state.local[name] === state.remote[name]);
  },
};

const apollo = {
  list: {
    dietType: "omnivore",
    favoriteCuisines: ["french"],
    favoriteIngredients: ["oat", "egg"],
    dailyCourses: ["sandwich", "soup", "snack", "dessert"],
  },
};

export const actions = {
  fetchList({ state, commit }) {
    if (Object.keys(state.remote).length === 0) {
      const list = apollo.list;
      commit("UPDATE_REMOTE", list);
      if (Object.keys(state.local).length === 0) {
        commit("UPDATE_LOCAL", Object.assign({}, list));
      }
    }
  },

  save({ state, commit }) {
    // const success = axios.post(state.local);
    const success = true;
    if (success) {
      commit("UPDATE_REMOTE", Object.assign({}, state.local));
    }
  },

  revert({ state, commit }) {
    commit("UPDATE_LOCAL", Object.assign({}, state.remote));
  },
};

export const mutations = {
  UPDATE_REMOTE(state, list) {
    state.remote = list;
  },

  UPDATE_LOCAL(state, list) {
    state.local = list;
  },

  UPDATE_ONE(state, { name, value }) {
    state.local[name] = value;
  },
};
