export const state = () => ({
  local: {},
  remote: {},
});

export const getters = {
  areSaved(state) {
    const names = Object.keys(state.local);
    return names.every((name) => state.local[name] === state.remote[name]);
  },
  bmi(state) {
    const result = state.local.weight / (state.local.height / 100) ** 2;
    const rounding = 2;
    return Math.round(result * 10 ** rounding) / 10 ** rounding || "unknown";
  },
  bmr(state) {
    const BMRwithAge = (sex, mass, height, age) => {
      if (sex === "male") {
        return 13.397 * mass + 4.799 * height - 5.677 * age + 88.362;
      } else if (sex === "female") {
        return 9.247 * mass + 3.098 * height - 4.33 * age + 447.593;
      }
    };
    const { sex, weight, height, birthDate } = state.local;
    const now = new Date();
    const birth = new Date(birthDate);
    let age = now.getFullYear() - birth.getFullYear();

    if (
      now.getMonth() < birth.getMonth() ||
      (now.getMonth() === birth.getMonth() && now.getDate() < birth.getDate())
    ) {
      age -= 1;
    }
    return BMRwithAge(sex, weight, height, age);
  },
  maintenanceCalories(state, getters) {
    const bmr = getters.bmr;
    switch (state.local.activity) {
      case 0:
        return bmr * 1.2;
      case 1:
        return bmr * 1.35;
      case 2:
        return bmr * 1.5;
      case 3:
        return bmr * 1.6;
      case 4:
        return bmr * 1.75;
      default:
        return bmr;
    }
  },
};

const apollo = {
  list: {
    height: 2390,
    weight: 14385,
    birthDate: new Date().getTime(),
    activity: 0,
    sex: "male",
    allergies: ["gluten", "peanut"],
    intolerances: ["egg", "soy", "dairy"],
  },
};

export const actions = {
  fetchList({ state, commit }) {
    if (Object.keys(state.remote).length === 0) {
      const list = apollo.list;
      commit("UPDATE_REMOTE", list);
      if (Object.keys(state.local).length === 0) {
        commit("UPDATE_LOCAL", Object.assign({}, list));
      }
    }
  },

  save({ state, commit }) {
    // const success = axios.post(state.local);
    const success = true;
    if (success) {
      commit("UPDATE_REMOTE", Object.assign({}, state.local));
    }
  },

  revert({ state, commit }) {
    commit("UPDATE_LOCAL", Object.assign({}, state.remote));
  },
};

export const mutations = {
  UPDATE_REMOTE(state, list) {
    state.remote = list;
  },

  UPDATE_LOCAL(state, list) {
    state.local = list;
  },

  UPDATE_ONE(state, { name, value }) {
    state.local[name] = value;
  },
};
