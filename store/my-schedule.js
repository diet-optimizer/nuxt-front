import json from "@/data/weekQueryExample.json";

const daysInWeek = [
  "sunday",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "saturday",
];

const monthsInYear = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "november",
  "december",
];

export const state = () => ({
  weeks: [],
  currentWeekIndex: 0,
});

const areSameDay = (date1, date2) => {
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
};

export const getters = {
  getDay: (state) => (date) => {
    const allFetchedDays = [].concat(...state.weeks);
    const day = allFetchedDays.find((_date) => areSameDay(_date.date, date));
    if (day !== undefined) {
      day.month = monthsInYear[date.getMonth()];
    }
    return day;
  },
  week(state) {
    return state.weeks[state.currentWeekIndex];
  },
  month(state) {
    const date = new Date();
    date.setDate(date.getDate() - 7 * state.currentWeekIndex);
    return monthsInYear[date.getMonth()];
  },
  year(state) {
    const date = new Date();
    date.setDate(date.getDate() - 7 * state.currentWeekIndex);
    return date.getFullYear();
  },
};

const getMondayOfTheWeek = (weekIndex) => {
  // function used for faking the request, should depend on : json.firstDay
  const date = new Date();
  date.setDate(date.getDate() - date.getDay() + 1 - weekIndex * 7);
  return date;
};

export const actions = {
  fetchWeek({ commit, $apollo }, weekIndex) {
    const mondayDate = getMondayOfTheWeek(weekIndex);

    const week = JSON.parse(JSON.stringify(json.days));
    week.forEach((day, index) => {
      const date = new Date(mondayDate);
      date.setDate(date.getDate() + index);
      day.date = date;
      day.dayInWeek = daysInWeek[date.getDay()];
      day.dayInMonth = date.getDate();
    });
    commit("SET_WEEK", { week, weekIndex });
  },
  fetchWeekByDate({ state, dispatch }, date) {
    const monday = getMondayOfTheWeek(0).setHours(0, 0, 0, 0);
    const temp = new Date(date).setHours(0, 0, 0, 0);
    const msInWeek = 7 * 24 * 60 * 60 * 1000;
    const weekDiff = Math.ceil((monday - temp) / msInWeek);
    if (!(weekDiff in state.weeks)) {
      dispatch("fetchWeek", weekDiff);
    }
  },
  nextWeek({ state, dispatch, commit }) {
    commit("SET_WEEK_INDEX", Math.max(0, state.currentWeekIndex - 1));
    if (!(state.currentWeekIndex in state.weeks)) {
      dispatch("fetchWeek", state.currentWeekIndex);
    }
  },
  previousWeek({ state, dispatch, commit }) {
    commit("SET_WEEK_INDEX", state.currentWeekIndex + 1);
    if (!(state.currentWeekIndex in state.weeks)) {
      dispatch("fetchWeek", state.currentWeekIndex);
    }
  },
};

export const mutations = {
  SET_WEEK(state, { week, weekIndex }) {
    state.weeks.splice(weekIndex, 1, week);
  },
  SET_WEEK_INDEX(state, index) {
    state.currentWeekIndex = index;
  },
};
