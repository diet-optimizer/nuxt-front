export const state = () => ({
  local: {},
  remote: {},
});

export const getters = {
  areSaved(state) {
    const settingsNames = Object.keys(state.local);
    return settingsNames.every(
      (setting) => state.local[setting] === state.remote[setting]
    );
  },
};

export const actions = {
  fetchSettings({ state, commit }) {
    if (Object.keys(state.remote).length === 0) {
      // const settings = axios.get(settings)
      const settings = {
        language: "en",
        username: "Jeff",
        email: "jeff@mail.com",
        notifications: true,
        colorTheme: "Gray",
      };
      commit("UPDATE_REMOTE", settings);
      if (Object.keys(state.local).length === 0) {
        commit("UPDATE_LOCAL", Object.assign({}, settings));
      }
    }
  },

  save({ state, commit, dispatch }) {
    // const success = axios.post(state.local);
    const success = true;
    if (success) {
      commit("UPDATE_REMOTE", Object.assign({}, state.local));
      const switchLocalePath = this.app.switchLocalePath(state.local.language);
      this.app.router.push(switchLocalePath);
    }
  },

  revert({ state, commit, dispatch }) {
    commit("UPDATE_LOCAL", Object.assign({}, state.remote));
  },
};

export const mutations = {
  UPDATE_REMOTE(state, settings) {
    state.remote = settings;
  },

  UPDATE_LOCAL(state, settings) {
    state.local = settings;
  },

  UPDATE_ONE(state, { name, value }) {
    state.local[name] = value;
  },
};
