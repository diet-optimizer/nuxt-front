export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: "Tasty Target",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || "",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon-white.ico" }],
  },
  /*
   ** Global CSS
   */
  css: ["~/assets/vars/main.scss"],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ["~/plugins/vue-masonry.client.js", "~/plugins/auth.js"],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module",
    "@nuxtjs/style-resources",
  ],
  styleResources: {
    scss: ["./assets/vars/*.scss", "./assets/abstracts/_mixins.scss"],
  },
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    "nuxt-buefy",
    "@nuxtjs/apollo",
    "nuxt-i18n",
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  apollo: {
    authenticationType: "JWT", // optional, default: 'Bearer'
    clientConfigs: {
      default: {
        // httpEndpoint: "http://rickandmortyapi.com/graphql",
        httpEndpoint: "http://localhost:8000/graphql/",
        // browserHttpEndpoint: "/graphql",
        tokenName: "apollo-token", // optional
      },
    },
  },

  i18n: {
    locales: [
      "en",
      "fr",
      // "zh"
    ],
    defaultLocale: "en",
    vueI18n: {
      fallbackLocale: "en",
      messages: {
        en: require("./locales/en.json"),
        fr: require("./locales/fr.json"),
        // zh: require("./locales/zh.json"),
      },
    },
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
};
