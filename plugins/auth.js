import Vue from "vue";
import loginQuery from "@/gql/login.gql";
import registerQuery from "@/gql/register.gql";
import meQuery from "@/gql/me.gql";

class Storage {
  constructor(defaultData) {
    this._state = new Vue({
      data: () => defaultData,
    });
  }

  get state() {
    return this._state;
  }

  getValue(key) {
    return this.state.$data[key];
  }

  setValue(key, value) {
    Vue.set(this.state, key, value);
  }

  removeValue(key) {
    this.setValue(key, null);
  }
}

export default ({ $apolloHelpers, app }, inject) => {
  const auth = {
    $apollo: app.apolloProvider.defaultClient,
    $apolloHelpers,
    $router: app.router,
    $storage: new Storage({ loggedIn: false, username: null, email: null }),
    $localePath: app.localePath,

    get loggedIn() {
      return this.$storage.getValue("loggedIn");
    },

    get username() {
      return this.$storage.getValue("username");
    },

    get email() {
      return this.$storage.getValue("email");
    },

    updateLoggedIn() {
      const hasToken = this.$apolloHelpers.getToken() !== undefined;
      this.$storage.setValue("loggedIn", hasToken);
      if (!hasToken) {
        this.$storage.removeValue("username");
        this.$storage.removeValue("email");
      }
    },

    async fetchUser() {
      const res = await this.$apollo
        .query({
          query: meQuery,
        })
        .then(({ data }) => data && data.me);
      if (res !== null) {
        this.$storage.setValue("username", res.username);
        this.$storage.setValue("email", res.email);
      } else {
        this.$storage.removeValue("username");
        this.$storage.removeValue("email");
      }
    },

    async login(email, password, nextUrl) {
      const res = await this.$apollo
        .mutate({
          mutation: loginQuery,
          variables: { email, password },
        })
        .then(({ data }) => data && data.tokenAuth);
      await this.$apolloHelpers.onLogin(res.token, undefined, {
        sameSite: "strict",
      });
      if (!res.success) {
        throw Object.values(res.errors).map((error) => error[0].message);
      } else {
        this.updateLoggedIn();
        this.fetchUser();
        if (nextUrl !== undefined) {
          this.$router.push(this.$localePath(nextUrl));
        }
      }
    },

    async register(username, email, password1, password2, nextUrl) {
      const res = await this.$apollo
        .mutate({
          mutation: registerQuery,
          variables: { username, email, password1, password2 },
        })
        .then(({ data }) => data && data.register);
      await this.$apolloHelpers.onLogin(res.token, undefined, {
        sameSite: "strict",
      });
      if (!res.success) {
        throw Object.values(res.errors).map((error) => error[0].message);
      }
      this.updateLoggedIn();
      this.fetchUser();
      if (nextUrl !== undefined) {
        this.$router.push(nextUrl);
      }
    },

    async logout(nextUrl) {
      await this.$apolloHelpers.onLogout();
      this.updateLoggedIn();
      this.$router.push(this.$localePath(nextUrl || "/"));
    },
  };

  auth.updateLoggedIn();
  auth.fetchUser();
  inject("auth", auth);
};
