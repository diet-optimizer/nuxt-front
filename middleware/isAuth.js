import { ToastProgrammatic as Toast } from "buefy";

export default function ({ $auth, redirect }) {
  if (!$auth.loggedIn) {
    if (process.client) {
      Toast.open({
        duration: 5000,
        message: "Access denied, please log in",
        type: "is-danger",
      });
    }
    redirect("/");
  }
}
